
// Update: 6/09/18
#include "EmonLib.h"
#include <SPI.h>
#include <Ethernet.h>
#include <Adafruit_MQTT.h>
#include <Adafruit_MQTT_Client.h>

// MAC address for your Ethernet shield
byte mac[] = { 0xDE, 0xBE, 0xBE, 0xEF, 0xFE, 0xfD };
 
//0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED original
//0xDE, 0xED, 0xBA, 0xFE, 0xFE, 0xED other

/************************* MQTT Broker Setup *********************************/
EnergyMonitor energyMonitor;
EnergyMonitor energyMonitor1;
EnergyMonitor energyMonitor2;
EnergyMonitor energyMonitor3;
EnergyMonitor energyMonitor4;
#define mqtt_server      "172.16.0.232"
#define mqtt_serveport   1883
#define mqtt_username    ""
#define mqtt_password    ""
float voltajeRed = 220.0;
EthernetClient client;

// Setup the MQTT client class by passing in the WiFi client and MQTT server and login details.
Adafruit_MQTT_Client mqtt(&client, mqtt_server, mqtt_serveport, mqtt_username, mqtt_password);

// Setup a feed called 'MegaA00' for publishing.
Adafruit_MQTT_Publish MegaA00 = Adafruit_MQTT_Publish(&mqtt, "ControlElectrico");

//Bug workaround
void MQTT_connect();

void setup() {
  Serial.begin(115200);
  delay(10);
  
  Serial.println(F("Mega Adafrut MQTT rev 0"));
    
  while (Ethernet.begin(mac) != 1)
  {
    Serial.println("Error getting IP address via DHCP, trying again...");
    delay(15000);
  }

    Serial.println("IP address: "); Serial.println(Ethernet.localIP());
delay(5000);
  Serial.println("before MQTT connect");
  MQTT_connect();
  Serial.println("after MQTT connect");

  energyMonitor.current(0, 1.6);
  energyMonitor1.current(1, 1.6);
  energyMonitor2.current(2, 1.6);
  energyMonitor3.current(3, 1.6);
  energyMonitor4.current(4, 1.6);
}

String mydata;
 char char_array[30];
void loop() {
  float x=25;

  // Ensure the connection to the MQTT server is alive (this will make the first
  // connection and automatically reconnect when disconnected).  See the MQTT_connect
  // function definition further below.

  MQTT_connect();
double Irms = energyMonitor.calcIrms(1484);
double Irms1 = energyMonitor1.calcIrms(1484);
double Irms2 = energyMonitor2.calcIrms(1484);
double Irms3 = energyMonitor3.calcIrms(1484);
double Irms4 = energyMonitor4.calcIrms(1484);
 
  // Calculamos la potencia aparente
  double potencia =  Irms * voltajeRed/100;
  double potencia1 =  Irms1 * voltajeRed/100;
  double potencia2 =  Irms2 * voltajeRed/100;
  double potencia3 =  Irms3 * voltajeRed/100;
  double potencia4 =  Irms4 * voltajeRed/100;
  // Publish to MQTT
  Serial.print(F("\nSending value "));
  Serial.print("...");
  mydata = String("DE:AD:BE:EF:FE:ED")+","+"1"+ ","+potencia+","+"2"+ ","+potencia1+","+"3"+ ","+potencia2+","+"4"+ ","+potencia3+","+"5"+ ","+potencia4;
  mydata.toCharArray(char_array, 450);
  Serial.print(char_array);
  if (! MegaA00.publish(char_array)) {
    Serial.println(F("Failed"));
  } else {
    Serial.println(F("OK!"));
  }

  // ping the server to keep the mqtt connection alive
  if(! mqtt.ping()) {
    mqtt.disconnect();
  }
  
   delay(1000);
}

// Function to connect and reconnect as necessary to the MQTT server.
// Should be called in the loop function and it will take care if connecting.

void MQTT_connect() {
  int8_t ret;
  
  Serial.println("mqtt connect routine");
 
  // Stop if already connected.
  if (mqtt.connected()) {
    return;
  }

  Serial.print("Connecting to MQTT... ");

  while ((ret = mqtt.connect()) != 0) { // connect will return 0 for connected
       Serial.println(mqtt.connectErrorString(ret));
       Serial.println("Retrying MQTT connection in 5 seconds...");
       mqtt.disconnect();
       Serial.println("wait");
       delay(5000);  // wait 5 seconds
  }
  Serial.println("MQTT Connected!");
}